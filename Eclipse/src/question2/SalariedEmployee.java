package question2;
/**
 * @author Zacharie Makeen
 */
public class SalariedEmployee implements Employee {

	private int yearlySalary;
	/**
	 * Initializes yearlySalary.
	 *
	 * @param yearlySalary
	 */
	public SalariedEmployee(int yearlySalary) {

		this.yearlySalary = yearlySalary;
	}
	/**
	 * Returns the yearlySalary.
	 *
	 * @return yearlySalary
	 */
	public int getYearlyPay() {
		
		return this.yearlySalary;
	}
}
