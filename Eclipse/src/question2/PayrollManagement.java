package question2;
/**
 * @author Zacharie Makeen
 */
public class PayrollManagement {
	/**
	 * Creates objects and calls the getTotalExpenses method.
	 */
	public static void main(String[] args) {

		SalariedEmployee salariedEmployee1 = new SalariedEmployee(30000);
		SalariedEmployee salariedEmployee2 = new SalariedEmployee(25000);
		HourlyEmployee hourlyEmployee1 = new HourlyEmployee(20, 14);
		HourlyEmployee hourlyEmployee2 = new HourlyEmployee(18, 16);
		UnionizedHourlyEmployee unionizedHourlyEmployee = new UnionizedHourlyEmployee(23, 17, 5000);

		Employee[] employees = {salariedEmployee1, salariedEmployee2, hourlyEmployee1, hourlyEmployee2, unionizedHourlyEmployee};
		System.out.println(getTotalExpenses(employees));
	}
	/**
	 * Returns the total amount of yearly expenses.
	 *
	 * @param Employee[] input
	 * @return totalExpenses
	 */
	public static int getTotalExpenses(Employee[] input) {

		int totalExpenses = 0;
		for(int i = 0; i < input.length; i++) {

			totalExpenses += input[i].getYearlyPay();
		}
		return totalExpenses;
	}

}