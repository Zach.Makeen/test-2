package question2;
/**
 * @author Zacharie Makeen
 */
public interface Employee {
	/**
	 * Demands classes implementing this interface to use this method.
	 */
	public int getYearlyPay();
}