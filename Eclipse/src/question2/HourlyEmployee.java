package question2;
/**
 * @author Zacharie Makeen
 */
public class HourlyEmployee implements Employee {

	private int hoursWorkedWeekly;
	private int hourlyPay;
	/**
	 * Initializes hoursWorkedWeekly & hourlyPay.
	 *
	 * @param hoursWorkedWeekly & hourlyPay
	 */
	public HourlyEmployee(int hoursWorkedWeekly, int hourlyPay) {

		this.hoursWorkedWeekly = hoursWorkedWeekly;
		this.hourlyPay = hourlyPay;
	}
	/**
	 * Returns the yearly pay of hourly emplyees.
	 *
	 * @return yearly salary
	 */
	public int getYearlyPay() {
		
		return (this.hoursWorkedWeekly * this.hourlyPay * 52);
	}
}
