package question2;
/**
 * @author Zacharie Makeen
 */
public class UnionizedHourlyEmployee extends HourlyEmployee {

	private int pensionContribution;
	/**
	 * Initializes pensionContribution and the two fields from the parent class.
	 *
	 * @param hoursWorkedWeekly & hourlyPay & pensionContribution
	 */
	public UnionizedHourlyEmployee(int hoursWorkedWeekly, int hourlyPay, int pensionContribution) {

		super(hoursWorkedWeekly, hourlyPay);
		this.pensionContribution = pensionContribution;
	}
	/**
	 * Returns the yearly pay of unionized hourly emplyees.
	 *
	 * @return yearly salary
	 */
	public int getYearlyPay() {

		return (super.getYearlyPay() + this.pensionContribution);
	}
}