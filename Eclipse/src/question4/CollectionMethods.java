package question4;
import java.util.ArrayList;
import java.util.Collection;
import question3.*;

public class CollectionMethods {

	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets) {
		
		ArrayList<Planet> planetList = new ArrayList<Planet>();
        
        for(Planet planet : planets) {
            if(planet.getOrder() == 1 || planet.getOrder() == 2 || planet.getOrder() == 3) {
                planetList.add(planet);
            }
        }
        
        return planetList;
	}

}
